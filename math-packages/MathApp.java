
// You can't import two classes that have the same name!!!
//import bobjones.math.Math;
//import salleysmith.math.Math;

public class MathApp{

	public static void main(String[] args){

		bobjones.math.Math bobsMathHelper = new bobjones.math.Math();
		salleysmith.math.Math sallyesMathHelper = new salleysmith.math.Math();

		float radius = 7F;
		float diameter = bobsMathHelper.calculateDiameterFromRadius(radius);
		System.out.println("DIAMETER: " +  diameter);

		float height = 10;
		float width = 5;
		float area = sallyesMathHelper.calculateArea(height, width);
		System.out.println("AREA: " + area);

	}

}