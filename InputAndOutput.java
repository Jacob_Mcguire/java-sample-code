import java.util.Scanner;

public class InputAndOutput{

	public static void main(String[] args){

	Scanner keyboard = new Scanner(System.in);
	System.out.println("Enter some input!");
	String input = keyboard.nextLine();
	System.out.println("Here is you input as output :" + input);

	keyboard.nextLine();
	System.out.println("Enter your first and last name");
	String fullName = keyboard.nextLine();
	System.out.println("Hello " + fullName + "! Welcome to Java");
}
}